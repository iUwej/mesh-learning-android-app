package com.iUwej.meshlearning;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.iUwej.meshlearning.data.MeshDbAdapter;
import com.iUwej.meshlearning.data.MeshWord;
import com.iUwej.meshlearning.data.QuestionGroup;

public class WordListActivity extends SherlockFragmentActivity {
	
	private int groupId;
	private QuestionGroup group;
	private MeshDbAdapter db;
	private ActionBar mBar;
	

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle bundle) {
		// TODO Auto-generated method stub
		super.onCreate(bundle);
		setContentView(R.layout.activity_word_list);
		groupId=getIntent().getIntExtra("GROUP_ID", 1);
		db= new MeshDbAdapter(this);
		db.open();
		group=db.fetchQuestionGrpObj(groupId);
		db.close();
		setTitle(group.getGroupName());
		mBar= getSupportActionBar();
		mBar.setDisplayHomeAsUpEnabled(true);
		if(bundle==null){
			WordListFragment frag=WordListFragment.newInstance(groupId);
			getSupportFragmentManager().beginTransaction().replace(R.id.activity_words_content,frag).commit();
		}
		
	}
	
	private void showAddWindow(){
		NewWordFragment frag=NewWordFragment.newInstance(groupId);
		FragmentTransaction tras=getSupportFragmentManager().beginTransaction();
		tras.replace(R.id.activity_words_content,frag);
		tras.addToBackStack("addfrag");
		tras.commit();
		
		
	}

	/* (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockFragmentActivity#onCreateOptionsMenu(com.actionbarsherlock.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.activity_word_list, menu);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockFragmentActivity#onOptionsItemSelected(com.actionbarsherlock.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			Intent i= new Intent(this, QuestionsGroupActivity.class);
			i.putExtra("SUBJECT", 1);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			return true;
		case R.id.add_word:
			showAddWindow();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}
	
	public static class WordListFragment extends SherlockListFragment{
		
		private int groupId;
		
	  
	   public static  WordListFragment newInstance(int id){
		   WordListFragment frag= new WordListFragment();
		   frag.setGroupId(id);
		   return frag;
	   }

		/* (non-Javadoc)
		 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
		 */
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
			MeshDbAdapter db= new MeshDbAdapter(getSherlockActivity());
			db.open();
			ArrayList<MeshWord> wordList=db.fetchWordList(groupId);
			db.close();
			WordAdapter lstAdapter= new WordAdapter(getSherlockActivity(), wordList);
			setListAdapter(lstAdapter);
			//setListShown(true);
			
		}
		
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			View view=inflater.inflate(R.layout.word_list_frag,container,false);
			return view;
		}

		/**
		 * @return the groupId
		 */
		public int getGroupId() {
			return groupId;
		}

		/**
		 * @param groupId the groupId to set
		 */
		public void setGroupId(int groupId) {
			this.groupId = groupId;
		}
		
	}
	
	public static class NewWordFragment extends SherlockFragment{
		
		private Button btnSave;
		private EditText txtWord;
		
		private int groupId;
		public static NewWordFragment newInstance(int id){
			NewWordFragment frag=new NewWordFragment();
			frag.setGroupId(id);
			return frag;
		}
		private boolean isFilled(EditText view){
			return view.getEditableText()!=null && !(view.getEditableText().toString().equals(""));
		}
		
		
		/* (non-Javadoc)
		 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
		 */
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
			txtWord=(EditText)getView().findViewById(R.id.txt_add_word);
			btnSave=(Button)getView().findViewById(R.id.btn_save_word);
			btnSave.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(isFilled(txtWord)){
						MeshDbAdapter db= new MeshDbAdapter(getSherlockActivity());
						db.open();
						long id=db.addWord(groupId, txtWord.getEditableText().toString(),"Sample sentence");
						db.close();
						Toast.makeText(getSherlockActivity(), "Word Added to group with id "+id,Toast.LENGTH_SHORT).show();	
						getSherlockActivity().getSupportFragmentManager().beginTransaction().remove(NewWordFragment.this).commit();
					}
					else{
						Toast.makeText(getSherlockActivity(),"Error: Provide a word",Toast.LENGTH_SHORT).show();
					}
					
					
				}
			});
		}


		@Override
		
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			View view=inflater.inflate(R.layout.add_word_frag, container, false);
			return view;
		}
		/**
		 * @return the groupId
		 */
		public int getGroupId() {
			return groupId;
		}
		/**
		 * @param groupId the groupId to set
		 */
		public void setGroupId(int groupId) {
			this.groupId = groupId;
		}
	}
	
	

}
