package com.iUwej.meshlearning;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.iUwej.meshlearning.data.MeshSubject;

class SubjectAdapter extends ArrayAdapter<MeshSubject> implements AdapterView.OnItemClickListener{
	private ArrayList<MeshSubject> subjects;
	private Context mContext;
	public SubjectAdapter(Context context,ArrayList<MeshSubject> subjects){
		super(context,R.layout.list_item,subjects);
		this.subjects=subjects;
		mContext=context;
	}
	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return subjects.size();
	}
	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getItem(int)
	 */
	@Override
	public MeshSubject getItem(int position) {
		// TODO Auto-generated method stub
		return subjects.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return subjects.get(position).getSubject_id();
	}
	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view=convertView;
		if(view==null){
			view=((LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_item,null);
			
		}
		TextView lblSubject=(TextView)view.findViewById(R.id.txt_lst_item);
		lblSubject.setText(getItem(position).getName());
		return view;
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		MeshSubject sub=getItem(position);
		Toast.makeText(mContext, sub.getSubject_id()+" "+sub.getName(), Toast.LENGTH_SHORT).show();
		
	}
	
	
}