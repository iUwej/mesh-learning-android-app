package com.iUwej.meshlearning.data;

import android.database.sqlite.SQLiteDatabase;

/**
 * A single word in a wordlist. Will be spoken out by the Text-to-Speech
 * Engine.  Class used to create a table for the wordlist
 * */

public class MeshWord {
	
	//private properties for the meshwords
	private int wordId;
	private String word;
	private int groupId;
	private String sentence;
	
	
	
	
	public static final String TABLE_CREATE=" create table meshwords(" +
			"_id INTEGER PRIMARY KEY AUTOINCREMENT," +
			"_group_id INEGER not null,"+
			"word text not null," +
			"sentence text);";
	
	public MeshWord(int id,int grpId,String word,String sentence){
		wordId=id;
		groupId=grpId;
		this.word=word;
		this.sentence=sentence;
	}
	
	public MeshWord(int id,int grpId,String word){
		wordId=id;
		groupId=grpId;
		this.word=word;
	}
	
	
	/**
	 * @return the wordId
	 */
	public int getWordId() {
		return wordId;
	}

	/**
	 * @param wordId the wordId to set
	 */
	public void setWordId(int wordId) {
		this.wordId = wordId;
	}

	/**
	 * @return the word
	 */
	public String getWord() {
		return word;
	}

	/**
	 * @param word the word to set
	 */
	public void setWord(String word) {
		this.word = word;
	}

	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the sentence
	 */
	public String getSentence() {
		return sentence;
	}

	/**
	 * @param sentence the sentence to set
	 */
	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	public static void onCreate(SQLiteDatabase db){
		db.execSQL(TABLE_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
		db.execSQL("DROP TABLE IF EXISTS meshwords");
		onCreate(db);
	}

}
