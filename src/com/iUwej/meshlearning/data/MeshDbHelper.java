package com.iUwej.meshlearning.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MeshDbHelper extends SQLiteOpenHelper {
	
	public static final String DB_NAME="mesh_db";
	public static final int VERSION=1;
	
	
	public MeshDbHelper(Context context){
		super(context,DB_NAME,null,VERSION);
		
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		MeshSubject.onCreate(db);
		QuestionGroup.onCreate(db);
		Question.onCreate(db);
		MeshWord.onCreate(db);
	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		MeshWord.onUpgrade(db, oldVersion, newVersion);
		Question.onUpgrade(db, oldVersion, newVersion);
		QuestionGroup.onUpgrade(db, oldVersion, newVersion);
		MeshSubject.onUpgrade(db, oldVersion, newVersion);
	}

}
