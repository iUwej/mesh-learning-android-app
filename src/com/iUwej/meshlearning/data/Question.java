package com.iUwej.meshlearning.data;

import android.database.sqlite.SQLiteDatabase;

/**
 * Class used to create an instance of a question and the answer. Each question
 * belongs to a particular {@link QuestionGroup} and consequently a {@link MeshSubject}.
 * */

public class Question {
	private int questionId;
	private int groupId;
	private String text;
	private String answer;
	private String hint;
	
	
	public static final String TABLE_CREATE="create table questions(" +
			"_id INTEGER PRIMARY KEY AUTOINCREMENT," +
			"_group_id INTEGER not null,"+
			"question text not null," +
			"answer text not null," +
			"hint text);";
	
	
	
	public Question(int questionId, int groupId, String text, String answer) {
		super();
		this.questionId = questionId;
		this.groupId = groupId;
		this.text = text;
		this.answer = answer;
	}

	/**
	 * @return the questionId
	 */
	public int getQuestionId() {
		return questionId;
	}

	/**
	 * @param questionId the questionId to set
	 */
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * @return the hint
	 */
	public String getHint() {
		return hint;
	}

	/**
	 * @param hint the hint to set
	 */
	public void setHint(String hint) {
		this.hint = hint;
	}

	/**
	 * method called to create the subject table when the database is being created
	 * */
	public static void onCreate(SQLiteDatabase db){
		db.execSQL(TABLE_CREATE);
	}
	
	/**
	 * method called when upgrading a database from an old version to a new
	 * version
	 * */
	
	public static void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
		db.execSQL("DROP TABLE IF EXISTS questions");
		onCreate(db);
	}
	

}
