package com.iUwej.meshlearning.data;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class MeshDbAdapter {
	
	private Context context;
	private SQLiteDatabase db;
	private MeshDbHelper dbHelper;
	private DataFactory dataFactory;
	
	//register the row fields as constants
	public static final String ROW_ID="_id";
	public static final String DESCRIPTION="description";
	public static final String NAME="name";
	public static final String GROUP_ID="_group_id";
	public static final String WORD="word";
	public static final String SENTENCE="sentence";
	public static final String QUESTION="question";
	public static final String ANSWER="answer";
	public static final String HINT="hint";
	public static final String SUBJECT_ID="s_id";
	
	//register the table names as constants
	public static final String SUBJECTS_TABLE="subjects";
	public static final String QUESTIONS_TABLE="questions";
	public static final String WORDS_TABLE="meshwords";
	public static final String QUESTIONSGROUPS_TABLE="questions_groups";
	
	
	
	public MeshDbAdapter(Context context){
		this.context=context;
		dataFactory= new DataFactory();
	}
	
	/**
	 * Create the database if it does not exist or opens the database for 
	 * reading and writing if it does exist
	 * */
	public MeshDbAdapter open() throws SQLiteException{
		dbHelper= new MeshDbHelper(context);
		db=dbHelper.getWritableDatabase();
		return this;
	}
	
	/**
	 * Close the database resources after use
	 * */
	
	public void close(){
		if(dbHelper !=null)dbHelper.close();
	}
	
	//crud methods for the database tables in our database
	//Subject table
	public long  addSubject(String name,String desc){
		ContentValues cv= new ContentValues();
		cv.put(NAME, name);
		cv.put(DESCRIPTION, desc);
		return db.insert(SUBJECTS_TABLE,null,cv);
	}
	
	public boolean deleteSubject(int id){
		return db.delete(SUBJECTS_TABLE, ROW_ID+"="+id, null)>0;
	}
	
	public boolean editSubject(int id,String name,String desc){
		ContentValues values= new ContentValues();
		values.put(NAME, name);
		values.put(DESCRIPTION, desc);
		return db.update(SUBJECTS_TABLE, values, ROW_ID+"="+id, null)>0;
	}
	
	public Cursor fetchSubject(int id){
		String[] cols={ROW_ID,NAME,DESCRIPTION};
		Cursor cursor=db.query(SUBJECTS_TABLE, cols, ROW_ID+"="+id,null,null, null,null);
		if(cursor !=null)
			cursor.moveToFirst();
		return cursor;
	}
	
	public MeshSubject fetchSubjectObj(int id){
		Cursor cursor=fetchSubject(id);
		MeshSubject subject=dataFactory.createFromCursor(cursor);
		return subject;
	}
	
	public Cursor fetchAllSubjects(){
		String[] cols={ROW_ID,NAME,DESCRIPTION};
		Cursor cursor=db.query(SUBJECTS_TABLE, cols,null,null,null, null,null);
		return cursor;
	}
	
	public ArrayList<MeshSubject> fetchSubjectList(){
		ArrayList<MeshSubject> subjects= new ArrayList<MeshSubject>();
		Cursor cursor=fetchAllSubjects();
		//cursor.moveToPosition(-1);
		if(cursor !=null){
			while(cursor.moveToNext())
				subjects.add(dataFactory.createFromCursor(cursor));
		}
		return subjects;
	}
	
	//Question Group table
	public long addQuestionGroup(int sid,String name,String desc){
		ContentValues values= new ContentValues();
		values.put(SUBJECT_ID,sid);
		values.put(NAME, name);
		values.put(DESCRIPTION,desc);
		
		return db.insert(QUESTIONSGROUPS_TABLE, null, values);
	}
	
	public boolean delQuestionGrp(int id){
		return db.delete(QUESTIONSGROUPS_TABLE,ROW_ID+"="+id,null)>0;
	}
	public boolean editQuestionGrp(int id,int sid,String name,String desc){
		ContentValues values= new ContentValues();
		values.put(SUBJECT_ID, sid);
		values.put(NAME,name);
		values.put(DESCRIPTION, desc);
		return db.update(QUESTIONSGROUPS_TABLE, values, ROW_ID+"="+id,null)>0;
	}
	
	public Cursor fetchQuestionGrp(int id){
		String[] cols={ROW_ID,SUBJECT_ID,NAME,DESCRIPTION};
		Cursor cursor=db.query(QUESTIONSGROUPS_TABLE, cols,ROW_ID+"="+id, null, null,null,null);
		if(cursor !=null)
			cursor.moveToFirst();
		return cursor;
	}
	
	public QuestionGroup fetchQuestionGrpObj(int id){
		Cursor cursor=fetchQuestionGrp(id);
		QuestionGroup group= dataFactory.createQuestionGrp(cursor);
		return group;
	}
	
	public Cursor fetchQuestionGrps(){
		String[] cols={ROW_ID,SUBJECT_ID,NAME,DESCRIPTION};
		Cursor cursor=db.query(QUESTIONSGROUPS_TABLE, cols,null, null, null,null,null);
		return cursor;
	}
	public Cursor fetchQuestionsGrpBySubject(int id){
		String[] cols={ROW_ID,SUBJECT_ID,NAME,DESCRIPTION};
		Cursor cursor=db.query(QUESTIONSGROUPS_TABLE, cols,SUBJECT_ID+"="+id, null, null,null,null);
		return cursor;
	}
	
	public ArrayList<QuestionGroup> fetchSubjectGrps(int subjectId){
		ArrayList<QuestionGroup> grps= new ArrayList<QuestionGroup>();
		Cursor cursor=fetchQuestionsGrpBySubject(subjectId);
		if(cursor !=null){
			while(cursor.moveToNext())
				grps.add(dataFactory.createQuestionGrp(cursor));
		}
		return grps;
	}
	
	//Question
	public long addQuestion(int grpId,String question,String answer,String hint){
		ContentValues values = new ContentValues();
		values.put(GROUP_ID,grpId);
		values.put(QUESTION, question);
		values.put(ANSWER, answer);
		values.put(HINT, hint);
		return db.insert(QUESTIONS_TABLE, null, values);
		
	}
	public boolean deleteQuestion(int id){
		return db.delete(QUESTIONS_TABLE,ROW_ID+"="+id, null)>0;
		
	}
	
	public boolean editQuestion(int id ,int grpId,String question,String answer,String hint){
		ContentValues values = new ContentValues();
		values.put(GROUP_ID,grpId);
		values.put(QUESTION, question);
		values.put(ANSWER, answer);
		values.put(HINT, hint);
		return db.update(QUESTIONS_TABLE, values, ROW_ID+"="+id, null)>0;
	}
	
	public Cursor fetchQuestion(int id){
		String cols[]={ROW_ID,GROUP_ID,QUESTION,ANSWER,HINT};
		Cursor cursor=db.query(QUESTIONS_TABLE, cols, ROW_ID+"="+id,null,null,null,null);
		if(cursor !=null)
			cursor.moveToFirst();
		return cursor;
	}
	
	public Cursor fetchQuestionsByGroup(int id){
		String cols[]={ROW_ID,GROUP_ID,QUESTION,ANSWER,HINT};
		Cursor cursor=db.query(QUESTIONS_TABLE, cols, GROUP_ID+"="+id,null,null,null,null);
		return cursor;
	}
	
	//Spellig list goes here
	public long addWord(int grpId,String word,String sentence){
		ContentValues values= new ContentValues();
		values.put(GROUP_ID, grpId);
		values.put(WORD, word);
		values.put(SENTENCE, sentence);
		
		return db.insert(WORDS_TABLE, null, values);
	}
	
	public boolean deleteWord(int id){
		return db.delete(WORDS_TABLE,ROW_ID+"="+id,null)>0;
	}
	
	public boolean editWord(int id,int grpId,String word,String sentence){
		ContentValues values= new ContentValues();
		values.put(GROUP_ID, grpId);
		values.put(WORD, word);
		values.put(SENTENCE, sentence);
		return db.update(WORDS_TABLE, values, ROW_ID+"="+id,null)>0;
	}
	
	public Cursor fetchWord(int id){
		String[] cols={ROW_ID,GROUP_ID,WORD,SENTENCE};
		Cursor cursor= db.query(WORDS_TABLE, cols, ROW_ID+"="+id,null,null,null,null);
		if(cursor !=null)
			cursor.moveToFirst();
		return cursor;
	}
	
	public Cursor fetchWordByGrp(int id){
		String[] cols={ROW_ID,GROUP_ID,WORD,SENTENCE};
		Cursor cursor= db.query(WORDS_TABLE, cols, GROUP_ID+"="+id,null,null,null,null);
		return cursor;
	}
	
	public ArrayList<MeshWord> fetchWordList(int grpId){
		Cursor cursor=fetchWordByGrp(grpId);
		ArrayList<MeshWord> list=null;
		if(cursor !=null){
			list= new ArrayList<MeshWord>();
			while(cursor.moveToNext())
				list.add(dataFactory.createMeshWord(cursor));
		}
		return list;
		
	}
	
	/**
	 * A factory class for creating new data objects from the database Cursors. Used as a helper
	 * for the database adapter.
	 * */
	
	class DataFactory{
		
		public MeshSubject createFromCursor(Cursor c){
			
			MeshSubject subject= new MeshSubject(c.getInt(0),c.getString(1),c.getString(2));
			return subject;
		}
		
		public QuestionGroup createQuestionGrp(Cursor c){
			QuestionGroup group= new QuestionGroup(c.getInt(0),c.getInt(1),c.getString(2),c.getString(3));
			return group;
		}
		
		public MeshWord createMeshWord(Cursor cursor){
			MeshWord word= new MeshWord(cursor.getInt(0), cursor.getInt(1),cursor.getString(2),cursor.getString(3));
			return word;
		}
	}

}
