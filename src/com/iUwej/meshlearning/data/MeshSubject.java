package com.iUwej.meshlearning.data;



import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 *@author iUwej<br>
 *This class used to create the Subjects table. Contains two static 
 *methods {@link #onCreate(SQLiteDatabase)} and {@link #onUpgrade(SQLiteDatabase, int, int)}
 *to be called by the {@link SQLiteOpenHelper} equivalent. 
 * */

public class MeshSubject {
	private int subject_id;
	private String name;
	private String description;
	public static final String DB_CREATE="create table subjects("
			+"_id INTEGER PRIMARY KEY AUTOINCREMENT,"
			+"name text not null,"
			+"description text not null);";
	
	
	
	public MeshSubject(int subject_id, String name, String description) {
		super();
		this.subject_id = subject_id;
		this.name = name;
		this.description = description;
	}

	public MeshSubject(int subject_id, String name) {
		super();
		this.subject_id = subject_id;
		this.name = name;
	}
	
	public String toString(){
		return name;
	}
	

	/**
	 * @return the subject_id
	 */
	public int getSubject_id() {
		return subject_id;
	}

	/**
	 * @param subject_id the subject_id to set
	 */
	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 *Method called by the to create a new subject table 
	 * */
	
	public static void onCreate(SQLiteDatabase db){
		db.execSQL(DB_CREATE);
	}
	
	/**
	 *Method called by the to handle the upgrade of our DB schema to a new one 
	 * */
	
	public static void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
		db.execSQL("DROP TABLE IF EXISTS subjects");
		onCreate(db);
	}
}
