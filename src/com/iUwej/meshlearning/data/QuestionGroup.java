package com.iUwej.meshlearning.data;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author iUwej
 * @email wawerumunene@gmail.com
 * <br/>
 * This class is used in grouping questions. Useful when questions are 
 * grouped into levels say in difficulty tiers. Contains two static 
 *methods {@link #onCreate(SQLiteDatabase)} and {@link #onUpgrade(SQLiteDatabase, int, int)}
 *to be called by the {@link SQLiteOpenHelper} equivalent.
 * */

public class QuestionGroup {
	private int groupId;
	private String groupName;
	private String description;
	private int subjectId;
	
	
	public static final String TABLE_CREATE="create table questions_groups(" +
			"_id INTEGER PRIMARY KEY AUTOINCREMENT," +
			"s_id INTEGER not null,"+
			"name text not null," +
			"description text not null);";
	
	
	/**
	 * @return the subjectId
	 */
	public int getSubjectId() {
		return subjectId;
	}
	
	@Override
	public String toString() {
		return groupName;
	}

	/**
	 * @param subjectId the subjectId to set
	 */
	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public QuestionGroup(int groupId,int subjectId, String groupName, String description) {
		super();
		this.groupId = groupId;
		this.groupName = groupName;
		this.description = description;
		this.subjectId=subjectId;
	}

	public QuestionGroup(int groupId, String groupName) {
		super();
		this.groupId = groupId;
		this.groupName = groupName;
	}

	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Method called by the DBHelper to create the questions_groups table
	 * */
	
	public static void onCreate(SQLiteDatabase db){
		db.execSQL(TABLE_CREATE);
	}
	
	/**
	 * Method called by the DBHelper when upgrading the database from old
	 * to a new version
	 * */
	public static void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
		Log.d("Questions Groups", "Upgrading db from ver "+oldVersion);
		db.execSQL("DROP TABLE IF EXISTS questions_groups");
		onCreate(db);
		Log.d("Questions Groups","Database upgraded to "+newVersion);
		
	}
	

}
