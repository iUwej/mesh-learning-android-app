package com.iUwej.meshlearning;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class TutorBoard extends Activity implements View.OnClickListener {

	private Button btnSpelling,btnEnglish,btnMaths;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tutor_board);
		btnSpelling=(Button)findViewById(R.id.btn_manage_spelling);
		btnEnglish=(Button)findViewById(R.id.btn_manage_english);
		btnMaths=(Button)findViewById(R.id.btn_manage_maths);
		btnEnglish.setOnClickListener(this);
		btnMaths.setOnClickListener(this);
		btnSpelling.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tutor_board, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.equals(btnSpelling)){
			Intent intent= new Intent(this,QuestionsGroupActivity.class);
			//intent.putExtra("ACTOR", actor);
			intent.putExtra("SUBJECT",1);
			startActivity(intent);
		}
		else if(v.equals(btnEnglish)){
			Intent intent= new Intent(this,QuestionsGroupActivity.class);
			//intent.putExtra("ACTOR", actor);
			intent.putExtra("SUBJECT",2);
			startActivity(intent);
		}
		else if(v.equals(btnMaths)){
			Intent intent= new Intent(this,QuestionsGroupActivity.class);
			//intent.putExtra("ACTOR", actor);
			intent.putExtra("SUBJECT",3);
			startActivity(intent);
		}
	}

}
