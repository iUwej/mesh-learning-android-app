
package com.iUwej.meshlearning;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.iUwej.meshlearning.data.MeshDbAdapter;
import com.iUwej.meshlearning.data.MeshSubject;

public class SubjectsActivity extends SherlockFragmentActivity {
	
	private ActionBar actionBar;
	private ListView lstSubjects;
	private long actor;

	/* (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockFragmentActivity#onCreateOptionsMenu(com.actionbarsherlock.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.subjects,menu);
		return true;
	}
	
	

	/* (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockFragmentActivity#onOptionsItemSelected(com.actionbarsherlock.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			Intent i= new Intent(this, MainActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			return true;
		case R.id.menu_add_subject:
			NewSubjectFragment frag= new NewSubjectFragment();
			frag.show(getSupportFragmentManager(), "addsubject");
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}



	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle bundle) {
		// TODO Auto-generated method stub
		super.onCreate(bundle);
		setContentView(R.layout.activity_subjects);
		setTitle("Subjects");
		actionBar=getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		lstSubjects=(ListView)findViewById(R.id.lst_subjects);
		setListAdapter();
		actor=getIntent().getLongExtra("ACTOR", 1);
		
	}
	
	private void setListAdapter(){
		MeshDbAdapter adapter= new MeshDbAdapter(this);
		adapter.open();
		ArrayList<MeshSubject> subs=adapter.fetchSubjectList();
		adapter.close();
		SubjectAdapter listAdapter= new SubjectAdapter(subs);
		lstSubjects.setAdapter(listAdapter);
		lstSubjects.setOnItemClickListener(listAdapter);
	}
	
	public static class NewSubjectFragment extends SherlockDialogFragment{
		private EditText txtName,txtDesc;
		private Button btnSave;

		/* (non-Javadoc)
		 * @see android.support.v4.app.DialogFragment#onActivityCreated(android.os.Bundle)
		 */
		@Override
		public void onActivityCreated(Bundle bundle) {
			// TODO Auto-generated method stub
			super.onActivityCreated(bundle);
			txtName=(EditText)getView().findViewById(R.id.txt_sub_name);
			txtDesc=(EditText)getView().findViewById(R.id.txt_sub_desc);
			btnSave=(Button)getView().findViewById(R.id.btn_sub_add);
			btnSave.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(isFilled(txtDesc) && isFilled(txtName)){
						String name=txtName.getEditableText().toString();
						String desc=txtDesc.getEditableText().toString();
						MeshDbAdapter adapter= new MeshDbAdapter(getSherlockActivity());
						adapter.open();
						adapter.addSubject(name, desc);
						adapter.close();
						SubjectsActivity act=(SubjectsActivity)getSherlockActivity();
						act.setListAdapter();
						
					}
					else{
						Toast.makeText(getSherlockActivity(), "Enter name and description for subject",Toast.LENGTH_SHORT).show();
					}
					dismiss();
				}
			});
			
		}

		/* (non-Javadoc)
		 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
		 */
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			View view=inflater.inflate(R.layout.add_subject_frag, container, false);
			return view;
		}
		
		private boolean isFilled(EditText view){
			return view.getEditableText()!=null && !(view.getEditableText().toString().equals(""));
		}
		
		
	}
	
	class SubjectAdapter extends ArrayAdapter<MeshSubject> implements AdapterView.OnItemClickListener{
		private ArrayList<MeshSubject> subjects;
		public SubjectAdapter(ArrayList<MeshSubject> subjects){
			super(SubjectsActivity.this,R.layout.list_item,subjects);
			this.subjects=subjects;
		}
		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getCount()
		 */
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return subjects.size();
		}
		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getItem(int)
		 */
		@Override
		public MeshSubject getItem(int position) {
			// TODO Auto-generated method stub
			return subjects.get(position);
		}
		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getItemId(int)
		 */
		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return subjects.get(position).getSubject_id();
		}
		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View view=convertView;
			if(view==null){
				view=getLayoutInflater().inflate(R.layout.list_item, null);
				
			}
			TextView lblSubject=(TextView)view.findViewById(R.id.txt_lst_item);
			lblSubject.setText(getItem(position).getName());
			return view;
		}
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			MeshSubject sub=getItem(position);
			Toast.makeText(getApplicationContext(), sub.getSubject_id()+" "+sub.getName(), Toast.LENGTH_SHORT).show();
			Intent intent= new Intent(SubjectsActivity.this,QuestionsGroupActivity.class);
			intent.putExtra("ACTOR", actor);
			intent.putExtra("SUBJECT", sub.getSubject_id());
			startActivity(intent);
			
		}
		
		
	}
	
	

}
