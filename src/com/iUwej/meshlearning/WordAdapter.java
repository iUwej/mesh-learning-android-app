package com.iUwej.meshlearning;

import java.util.ArrayList;

import com.iUwej.meshlearning.data.MeshWord;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class WordAdapter extends ArrayAdapter<MeshWord>{
	private ArrayList<MeshWord> wordList;
	private Context mContext;
	
	public WordAdapter(Context context,ArrayList<MeshWord> list){
		super(context,R.layout.list_item,list);
		wordList=list;
		mContext=context;
	}

	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return wordList.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getItem(int)
	 */
	@Override
	public MeshWord getItem(int position) {
		// TODO Auto-generated method stub
		return wordList.get(position);
	}

	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return getItem(position).getWordId();
	}

	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view=convertView;
		if(view==null){
			LayoutInflater inflater=(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view=inflater.inflate(R.layout.word_item, null);
		}
		TextView txtWord=(TextView)view.findViewById(R.id.txt_list_word);
		txtWord.setText(getItem(position).getWord());
		return view;
	}
	
	
	
	
	
	

}
