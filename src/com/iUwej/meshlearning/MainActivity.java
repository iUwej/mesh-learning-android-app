package com.iUwej.meshlearning;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import android.view.View;

public class MainActivity extends Activity implements OnClickListener{
	private Button btnTutor,btnStudents;
	public static final int ACTOR_STUDENT=1;
	public static final int ACTOR_TUTOR=2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btnTutor=(Button)findViewById(R.id.btn_tutor_entry);
		btnStudents=(Button)findViewById(R.id.btn_student_entry);
		btnTutor.setOnClickListener(this);
		btnStudents.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.equals(btnTutor)){
			Intent intent = new Intent(MainActivity.this,TutorBoard.class);
			intent.putExtra("ACTOR",ACTOR_TUTOR);
			startActivity(intent);
		}
		else if(v.equals(btnStudents)){
			Toast.makeText(getApplicationContext(), "Not Implemented",Toast.LENGTH_SHORT).show();
		}
	}

}
