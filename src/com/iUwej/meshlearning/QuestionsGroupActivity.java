package com.iUwej.meshlearning;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.iUwej.meshlearning.data.MeshDbAdapter;
import com.iUwej.meshlearning.data.MeshSubject;
import com.iUwej.meshlearning.data.QuestionGroup;

public class QuestionsGroupActivity extends SherlockFragmentActivity {
	
	private int actor;
	private int subjectId;
	private ListView lstGroups;
	private ActionBar actionBar;
	private MeshSubject subject;
	

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle bundle) {
		// TODO Auto-generated method stub
		super.onCreate(bundle);
		setContentView(R.layout.activity_subject_groups);
		actor=getIntent().getIntExtra("ACTOR", 1);
		subjectId=getIntent().getIntExtra("SUBJECT", 1);
		lstGroups= (ListView)findViewById(R.id.lst_subject_grps);
		actionBar=getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		MeshDbAdapter db= new MeshDbAdapter(this);
		db.open();
		subject=db.fetchSubjectObj(subjectId);
		db.close();
		setTitle("Questions-"+subject.getName());
		setAdapter();
		
	}
	
	private void setAdapter(){
		MeshDbAdapter adapter= new MeshDbAdapter(this);
		adapter.open();
		ArrayList<QuestionGroup> grps=adapter.fetchSubjectGrps(subjectId);
		
		adapter.close();
		GroupsAdapter grpAdapter= new GroupsAdapter(grps);
		lstGroups.setAdapter(grpAdapter);
		lstGroups.setOnItemClickListener(grpAdapter);
		
	}

	/* (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockFragmentActivity#onCreateOptionsMenu(com.actionbarsherlock.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.subject_grps, menu);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockFragmentActivity#onOptionsItemSelected(com.actionbarsherlock.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			Intent i= new Intent(this, TutorBoard.class);
			i.putExtra("ACTOR", actor);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			return true;
		case R.id.add_sub_grp:
			new NewQuestionGrpFragment().show(getSupportFragmentManager(), "NewGrp");
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}
	
	class GroupsAdapter extends ArrayAdapter<QuestionGroup> implements AdapterView.OnItemClickListener{
		
		private ArrayList<QuestionGroup> groups;
		
		public GroupsAdapter(ArrayList<QuestionGroup> data){
			super(QuestionsGroupActivity.this,R.layout.list_item,data);
			groups=data;
		}
		
		
		
		

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getCount()
		 */
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return groups.size();
		}





		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getItem(int)
		 */
		@Override
		public QuestionGroup getItem(int position) {
			// TODO Auto-generated method stub
			return groups.get(position);
		}



		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getItemId(int)
		 */
		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return getItem(position).getGroupId();
		}
		
		private String getDisplayTag(int position){
			return getItem(position).getGroupName();
		}



		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View view=convertView;
			if(view==null){
				view=getLayoutInflater().inflate(R.layout.list_item,null);
				
			}
			TextView textView= (TextView)view.findViewById(R.id.txt_lst_item);
			textView.setText(getDisplayTag(position));
			return view;
		}



		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			int grpId=(int)id;
			switch(subjectId){
			case 1:
				Intent intent= new Intent(QuestionsGroupActivity.this, WordListActivity.class);
				intent.putExtra("GROUP_ID",grpId);
				startActivity(intent);
				break;
			case 2:
				Intent i = new Intent(QuestionsGroupActivity.this,WordedQuestionsActivity.class);
				i.putExtra("SUBJECT", subjectId);
				i.putExtra("GROUP_ID", grpId);
				startActivity(i);
				break;
			case 3:
				Intent in = new Intent(QuestionsGroupActivity.this,WordedQuestionsActivity.class);
				in.putExtra("SUBJECT", subjectId);
				in.putExtra("GROUP_ID", grpId);
				startActivity(in);
				break;
			default:
				Toast.makeText(getApplicationContext(), "Coming soon", Toast.LENGTH_SHORT).show();
				
			}
			
		}
	}
	
	public static class NewQuestionGrpFragment extends SherlockDialogFragment{
		
		
		private Button btnAdd;
		private Spinner spSubjects;
		private EditText txtName,txtDesc;

		/* (non-Javadoc)
		 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
		 */
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			View view=inflater.inflate(R.layout.add_question_group, container, false);
			return view;
		}
		
	
		@Override
		public void onActivityCreated(Bundle bundle) {
			// TODO Auto-generated method stub
			super.onActivityCreated(bundle);
			spSubjects=(Spinner)getView().findViewById(R.id.sp_subjects);
			MeshDbAdapter adapter= new MeshDbAdapter(getSherlockActivity());
			adapter.open();
			SubjectAdapter spAdapter= new SubjectAdapter(getSherlockActivity(), adapter.fetchSubjectList());
			adapter.close();
			spSubjects.setAdapter(spAdapter);
			txtDesc=(EditText)getView().findViewById(R.id.txt_sub_grp_desc);
			txtName=(EditText)getView().findViewById(R.id.txt_sub_grp_name);
			
			btnAdd=(Button)getView().findViewById(R.id.btn_sub_grp_add);
			btnAdd.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(isFilled(txtName) && isFilled(txtDesc)){
						int subjectId=(int) spSubjects.getSelectedItemId();
						MeshDbAdapter adapter= new MeshDbAdapter(getSherlockActivity());
						adapter.open();
						adapter.addQuestionGroup(subjectId, txtName.getEditableText().toString(),txtDesc.getEditableText().toString());
						adapter.close();
					}
					else{
						
					}
					dismiss();
				}
			});
		}
		
		private boolean isFilled(EditText view){
			return view.getEditableText()!=null && !(view.getEditableText().toString().equals(""));
		}
		
	}
	

}
